const moment = require('moment');

const VOUCHER_ATTRS = [
  { attr: 'NoCertificado', key: 'voucherCertificateNumber' },
  { attr: 'LugarExpedicion', key: 'voucherExpeditionPlace' },
  { attr: 'MetodoPago', key: 'voucherPaymentMethod' },
  { attr: 'Confirmacion', key: 'voucherConfirmation' },
  { attr: 'Certificado', key: 'voucherCertificate' },
  { attr: 'TipoCambio', key: 'voucherExchangeRate' },
  { attr: 'TipoDeComprobante', key: 'voucherType' },
  { attr: 'FormaPago', key: 'voucherPaymentType' },
  { attr: 'Descuento', key: 'voucherDiscount' },
  { attr: 'Folio', key: 'voucherConsecutive' },
  { attr: 'Version', key: 'voucherVersion' },
  { attr: 'Moneda', key: 'voucherCurrency' },
  { attr: 'Serie', key: 'voucherSeries' },
  { attr: 'Sello', key: 'voucherSeal' },
  { attr: 'UUID', key: 'voucherUuid' },
  { attr: 'Descuento', key: 'voucherDiscount' },
  {
    attr: 'Fecha',
    key: 'voucherDate',
    cast: (value) => moment.unix(value).format('YYYY-MM-DDTHH:mm:ss'),
  },
  {
    attr: 'Total',
    key: 'voucherTotal',
    cast: (value) => parseFloat(value),
  },
  {
    attr: 'SubTotal',
    key: 'voucherSubTotal',
    cast: (value) => parseFloat(value),
  },
];

module.exports = VOUCHER_ATTRS;
