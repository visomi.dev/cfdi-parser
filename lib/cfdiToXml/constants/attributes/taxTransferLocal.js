const LOCAL_TAX_TRANSFER_ATTRS = [
  {
    attr: 'TasadeTraslado',
    key: 'fee',
    cast: (value) => parseFloat(value),
  },
  {
    attr: 'ImpLocTrasladado',
    key: 'tax',
    cast: (value) => parseFloat(value),
  },
  {
    attr: 'Importe',
    key: 'amount',
    cast: (value) => parseFloat(value),
  },
];

module.exports = LOCAL_TAX_TRANSFER_ATTRS;
