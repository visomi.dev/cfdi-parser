const LOCAL_TAXES_ATTRS = [
  {
    attr: 'TotaldeRetenciones',
    key: 'totalLocalTaxesWithheld',
    cast: (value) => parseFloat(value),
    default: 0.00,
  },
  {
    attr: 'TotaldeTraslados',
    key: 'totalLocalTaxesTransferred',
    cast: (value) => parseFloat(value),
    default: 0.00,
  },
  {
    attr: 'version',
    key: 'Version',
    default: 1.0,
  },
];

module.exports = LOCAL_TAXES_ATTRS;
