const PAYMENTS_ATTRS = [
  {
    attr: 'Version',
    key: 'version',
    default: 1.0,
  },
];

module.exports = PAYMENTS_ATTRS;
