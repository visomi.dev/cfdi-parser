const RECEIVER_ATTRS = [
  { attr: 'ResidenciaFiscal', key: 'receiverFiscalResidence' },
  { attr: 'NumRegIdTrib', key: 'receiverNumRegIdTrib' },
  { attr: 'UsoCFDI', key: 'receiverCfdiUsage' },
  { attr: 'Nombre', key: 'receiverName' },
  { attr: 'Rfc', key: 'receiverRfc' },
];


module.exports = RECEIVER_ATTRS;
