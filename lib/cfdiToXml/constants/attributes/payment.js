const moment = require('moment');

const PAYMENT_ATTRS = [
  { attr: 'CtaBeneficiario', key: 'receiverAccount' },
  { attr: 'TipoCadPago', key: 'stringPaymentType' },
  { attr: 'NumOperacion', key: 'operationNumber' },
  { attr: 'RfcEmisorCtaBen', key: 'receiverRfc' },
  { attr: 'CtaOrdenante', key: 'issuingAccount' },
  { attr: 'RfcEmisorCtaOrd', key: 'issuingRfc' },
  { attr: 'TipoCambioP', key: 'exchangeRate' },
  { attr: 'NomBancoOrdExt', key: 'bankName' },
  { attr: 'CertPago', key: 'certificate' },
  { attr: 'FormaDePagoP', key: 'wayPay' },
  { attr: 'MonedaP', key: 'currency' },
  { attr: 'CadPago', key: 'string' },
  { attr: 'SelloPago', key: 'seal' },
  {
    attr: 'Monto',
    key: 'amount',
    cast: (value) => parseFloat(value),
  },
  {
    attr: 'FechaPago',
    key: 'paymentDate',
    cast: (value) => moment.unix(value).format('YYYY-MM-DDTHH:mm:ss'),
  },
];

module.exports = PAYMENT_ATTRS;
