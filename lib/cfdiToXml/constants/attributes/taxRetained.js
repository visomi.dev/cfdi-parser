const TAX_RETAINED_ATTRS = [
  {
    attr: 'TipoFactor',
    key: 'factorType',
  },
  {
    attr: 'TasaOCuota',
    key: 'fee',
    cast: (value) => parseFloat(value),
  },
  {
    attr: 'Impuesto',
    key: 'tax',
    cast: (value) => parseFloat(value),
  },
  {
    attr: 'Base',
    key: 'base',
    cast: (value) => parseFloat(value),
  },
  {
    attr: 'Importe',
    key: 'amount',
    cast: (value) => parseFloat(value),
  },
];

module.exports = TAX_RETAINED_ATTRS;
