const DOCUMENT_RELATED_ATTRS = [
  { attr: 'IdDocumento', key: 'documentId' },
  { attr: 'Serie', key: 'documentSeries' },
  { attr: 'Folio', key: 'documentConsecutive' },
  { attr: 'ImpSaldoAnt', key: 'documentPreviousBalance' },
  { attr: 'MonedaDR', key: 'documentCurrency' },
  { attr: 'TipoCambioDR', key: 'documentExchangeRate' },
  { attr: 'MetodoDePagoDR', key: 'documentPaymentMethod' },
  { attr: 'NumParcialidad', key: 'documentPartialityNumber' },
  { attr: 'ImpPagado', key: 'documentAmountPaid' },
  { attr: 'ImpSaldoInsoluto', key: 'documentOutstandingBalance' },
];


module.exports = DOCUMENT_RELATED_ATTRS;
