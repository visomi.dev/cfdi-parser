const CONCEPT_ATTRS = [
  { attr: 'NoIdentificacion', key: 'identificationNumber' },
  { attr: 'ClaveProdServ', key: 'productServiceKey' },
  { attr: 'Descripcion', key: 'description' },
  { attr: 'ClaveUnidad', key: 'unitKey' },
  {
    attr: 'ValorUnitario',
    key: 'unit',
    cast: (value) => parseFloat(value),
  },
  {
    attr: 'Descuento',
    key: 'discount',
    cast: (value) => parseFloat(value),
  },
  {
    attr: 'Cantidad',
    key: 'quantity',
    cast: (value) => parseInt(value, 10),
  },
  {
    attr: 'Importe',
    key: 'amount',
    cast: (value) => parseFloat(value),
  },
];

module.exports = CONCEPT_ATTRS;
