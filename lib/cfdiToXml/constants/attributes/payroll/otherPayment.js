const PAYROLL_OTHER_PAYMENT_ATTRS = [
  { attr: 'TipoOtroPago', key: 'otherPaymentType' },
  { attr: 'Clave', key: 'key' },
  { attr: 'Concepto', key: 'concept' },
  { attr: 'Importe', key: 'amount' },
];

module.exports = PAYROLL_OTHER_PAYMENT_ATTRS;
