const moment = require('moment');

const PAYROLL_RECEIVER_ATTRS = [
  { attr: 'Curp', key: 'receiverCurp' },
  { attr: 'NumSeguridadSocial', key: 'receiverSocialSecurityNumber' },
  {
    attr: 'FechaInicioRelLaboral',
    key: 'receiverJobRelationshipStart',
    cast: (value) => moment.unix(value).format('YYYY-MM-DD'),
  },
  { attr: 'Antigüedad', key: 'receiverSeniority' },
  { attr: 'TipoContrato', key: 'receiverContractType' },
  { attr: 'Sindicalizado', key: 'receiverSyndicalized' },
  { attr: 'TipoJornada', key: 'receiverWorkingDayType' },
  { attr: 'TipoRegimen', key: 'receiverRegimeType' },
  { attr: 'NumEmpleado', key: 'receiverEmployedNumber' },
  { attr: 'Departamento', key: 'receiverDepartment' },
  { attr: 'Puesto', key: 'receiverPosition' },
  { attr: 'RiesgoPuesto', key: 'receiverHazardPosition' },
  { attr: 'PeriodicidadPago', key: 'receiverPaymentPeriodicity' },
  { attr: 'Banco', key: 'receiverBank' },
  { attr: 'CuentaBancaria', key: 'receiverBankAccount' },
  { attr: 'SalarioBaseCotApor', key: 'receiverBaseSalary' },
  { attr: 'SalarioDiarioIntegrado', key: 'receiverDailyBaseSalary' },
  { attr: 'ClaveEntFed', key: 'receiverLocalityKey' },
];

module.exports = PAYROLL_RECEIVER_ATTRS;
