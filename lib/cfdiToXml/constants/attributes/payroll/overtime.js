const PAYROLL_OVERTIME_ATTRS = [
  { attr: 'Dias', key: 'days' },
  { attr: 'TipoHoras', key: 'hoursType' },
  { attr: 'HorasExtra', key: 'ovetime' },
  { attr: 'ImportePagado', key: 'amount' },
];


module.exports = PAYROLL_OVERTIME_ATTRS;
