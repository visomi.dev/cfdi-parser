const PAYROLL_DEDUCTION_ATTRS = [
  { attr: 'TipoDeduccion', key: 'deductionType' },
  { attr: 'Clave', key: 'key' },
  { attr: 'Concepto', key: 'concept' },
  { attr: 'Importe', key: 'amount' },
];

module.exports = PAYROLL_DEDUCTION_ATTRS;
