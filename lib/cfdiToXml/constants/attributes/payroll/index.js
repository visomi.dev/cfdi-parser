const moment = require('moment');

const PAYROLL_ATTRS = [
  { attr: 'Version', key: 'payrollVersion' },
  { attr: 'TipoNomina', key: 'payrollType' },
  {
    attr: 'FechaPago',
    key: 'payrollPaymentDate',
    cast: (value) => moment.unix(value).format('YYYY-MM-DD'),
  },
  {
    attr: 'FechaInicialPago',
    key: 'payrollPaymentPeriodStart',
    cast: (value) => moment.unix(value).format('YYYY-MM-DD'),
  },
  {
    attr: 'FechaFinalPago',
    key: 'payrollPaymentPeriodEnd',
    cast: (value) => moment.unix(value).format('YYYY-MM-DD'),
  },
  { attr: 'NumDiasPagados', key: 'payrollEarnedDays' },
  { attr: 'TotalPercepciones', key: 'payrollTotalPercepts' },
  { attr: 'TotalDeducciones', key: 'payrollTotalDeductions' },
  { attr: 'TotalOtrosPagos', key: 'payrollTotalOtherPayments' },
];

module.exports = PAYROLL_ATTRS;
