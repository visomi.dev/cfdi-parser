const PAYROLL_DEDUCTIONS_ATTRS = [
  { attr: 'TotalOtrasDeducciones', key: 'deductionsTotalOtherDeductions' },
  { attr: 'TotalImpuestosRetenidos', key: 'deductionsTotalTaxWithheld' },
];

module.exports = PAYROLL_DEDUCTIONS_ATTRS;
