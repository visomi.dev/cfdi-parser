const PAYROLL_EMITTER_ATTRS = [
  { attr: 'Curp', key: 'emitterCurp' },
  { attr: 'RegistroPatronal', key: 'emitterEmployerRegistration' },
  { attr: 'RfcPatronOrigen', key: 'emitterOriginEmployerRfc' },
];


module.exports = PAYROLL_EMITTER_ATTRS;
