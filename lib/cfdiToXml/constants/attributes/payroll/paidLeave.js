const PAYROLL_PAID_LEAVE_ATTRS = [
  { attr: 'DiasIncapacidad', key: 'daysOfPaidLeave' },
  { attr: 'TipoIncapacidad', key: 'paidLeaveType' },
  { attr: 'ImporteMonetario', key: 'amount' },
];

module.exports = PAYROLL_PAID_LEAVE_ATTRS;
