const PAYROLL_PERCEPTS_ATTRS = [
  { attr: 'TotalSueldos', key: 'perceptsTotalEarnings' },
  { attr: 'TotalSeparacionIndemnizacion', key: 'perceptsTotalCompensation' },
  { attr: 'TotalJubilacionPensionRetiro', key: 'perceptsTotalRetirementBenefit' },
  { attr: 'TotalGravado', key: 'perceptsTotalTaxed' },
  { attr: 'TotalExento', key: 'perceptsTotalExempt' },
];

module.exports = PAYROLL_PERCEPTS_ATTRS;
