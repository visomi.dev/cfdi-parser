const PAYROLL_COMPENSATION_ATTRS = [
  { attr: 'TotalPagado', key: 'totalPaid' },
  { attr: 'NumAñosServicio', key: 'seniorityServedYears' },
  { attr: 'UltimoSueldoMensOrd', key: 'lastMonthlyEarnings' },
  { attr: 'IngresoAcumulable', key: 'cumulableEarnings' },
  { attr: 'IngresoNoAcumulable', key: 'nonCumulableEarnings' },
];

module.exports = PAYROLL_COMPENSATION_ATTRS;
