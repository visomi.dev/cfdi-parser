const PAYROLL_RETIREMENT_ATTRS = [
  { attr: 'TotalUnaExhibicion', key: 'totalSinglePayment' },
  { attr: 'TotalParcialidad', key: 'totalPaymentInstalment' },
  { attr: 'MontoDiario', key: 'dailyAmount' },
  { attr: 'IngresoAcumulable', key: 'cumulableEarningAmount' },
  { attr: 'IngresoNoAcumulable', key: 'nonCumulableEarningAmount' },
];

module.exports = PAYROLL_RETIREMENT_ATTRS;
