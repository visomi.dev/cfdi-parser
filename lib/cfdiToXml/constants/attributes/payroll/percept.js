const PAYROLL_PERCEPT_ATTRS = [
  { attr: 'TipoPercepcion', key: 'perceptType' },
  { attr: 'Clave', key: 'key' },
  { attr: 'Concepto', key: 'concept' },
  { attr: 'ImporteGravado', key: 'taxedAmount' },
  { attr: 'ImporteExento', key: 'exemptAmount' },
];

module.exports = PAYROLL_PERCEPT_ATTRS;
