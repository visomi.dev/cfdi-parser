const TAXES_ATTRS = [
  {
    attr: 'TotalImpuestosRetenidos',
    key: 'totalTaxesWithheld',
    cast: (value) => parseFloat(value),
  },
  {
    attr: 'TotalImpuestosTrasladados',
    key: 'totalTaxesTransferred',
    cast: (value) => parseFloat(value),
  },
];

module.exports = TAXES_ATTRS;
