const moment = require('moment');

const DIGITAL_TAX_STAMP_ATTRS = [
  { attr: 'NoCertificadoSAT', key: 'satCertificateNumber' },
  { attr: 'RfcProvCertif', key: 'rfcProvCertif' },
  { attr: 'SelloCFD', key: 'cfdSeal' },
  { attr: 'SelloSAT', key: 'satSeal' },
  { attr: 'Version', key: 'version' },
  { attr: 'Leyenda', key: 'legend' },
  { attr: 'UUID', key: 'uuid' },
  {
    attr: 'FechaTimbrado',
    key: 'datetimeStamp',
    cast: (value) => moment.unix(value).format('YYYY-MM-DDTHH:mm:ss'),
  },
];

module.exports = DIGITAL_TAX_STAMP_ATTRS;
