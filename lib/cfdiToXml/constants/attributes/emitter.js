const EMITTER_ATTRS = [
  { attr: 'RegimenFiscal', key: 'emitterRegime' },
  { attr: 'Nombre', key: 'emitterName' },
  { attr: 'Rfc', key: 'emitterRfc' },
];

module.exports = EMITTER_ATTRS;
