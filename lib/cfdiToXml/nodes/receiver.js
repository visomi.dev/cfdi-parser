const mapAttrs = require('../../utils/mapAttrs');

const receiverAttrs = require('../constants/attributes/receiver');

function setReceiverAttrs(cfdi, voucherNode) {
  const receiverNode = voucherNode.node('Receptor');
  const receiverNodeAttrs = receiverAttrs.reduce(mapAttrs(cfdi), {});

  receiverNode.attr(receiverNodeAttrs);
  receiverNode.namespace('cfdi');

  return voucherNode;
}

module.exports = setReceiverAttrs;
