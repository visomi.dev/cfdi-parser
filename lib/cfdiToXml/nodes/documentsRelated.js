const mapAttrs = require('../../utils/mapAttrs');

const documentRelatedAttrs = require('../constants/attributes/documentRelated');

function setDocumentsRelated(payment, paymentNode) {
  if (!payment.documentsRelated && !Array.isArray(payment.documentsRelated)) return paymentNode;

  const { documentsRelated } = payment;

  documentsRelated.forEach((documentRelated) => {
    const documentRelatedNode = paymentNode.node('DoctoRelacionado');

    documentRelatedNode.namespace('pago10');

    const documentRelatedNodeAttrs = documentRelatedAttrs.reduce(mapAttrs(documentRelated), {});

    documentRelatedNode.attr(documentRelatedNodeAttrs);
  });

  return paymentNode;
}

module.exports = setDocumentsRelated;
