const mapAttrs = require('../../utils/mapAttrs');

const setDocumentsRelated = require('./documentsRelated');

const paymentsAttrs = require('../constants/attributes/payments');
const paymentAttrs = require('../constants/attributes/payment');

function setPayments(payments, complementNode) {
  const paymentsNode = complementNode.node('Pagos');

  paymentsNode.defineNamespace('pago10', 'http://www.sat.gob.mx/Pagos');
  paymentsNode.namespace('pago10');

  const paymentsNodeAttrs = paymentsAttrs.reduce(mapAttrs(payments), {});

  paymentsNode.attr(paymentsNodeAttrs);

  payments.forEach((payment) => {
    const paymentNode = paymentsNode.node('Pago');

    paymentNode.namespace('pago10');

    const paymentNodeAttrs = paymentAttrs.reduce(mapAttrs(payment), {});

    paymentNode.attr(paymentNodeAttrs);

    if (payment.documentsRelated) setDocumentsRelated(payment, paymentNode);
  });

  return complementNode;
}

module.exports = setPayments;
