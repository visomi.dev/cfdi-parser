const setDigitalTaxStamp = require('./digitalTaxStamp');
const setPayments = require('./payments');
const setLocalTaxes = require('./localTaxes');
const setPayroll = require('./payroll');

function setComplement(cfdi, voucherNode) {
  if (!cfdi.complement) return voucherNode;

  const {
    digitalTaxStamp,
    payments,
    payroll,
    localTaxes,
  } = cfdi.complement;

  const complementNode = voucherNode.node('Complemento');
  complementNode.namespace('cfdi');

  if (digitalTaxStamp) setDigitalTaxStamp(digitalTaxStamp, complementNode);

  const havePayments = payments && Array.isArray(payments);

  if (havePayments) setPayments(payments, complementNode);

  if (localTaxes) setLocalTaxes(localTaxes, complementNode);

  if (payroll) setPayroll(payroll, complementNode);

  return voucherNode;
}

module.exports = setComplement;
