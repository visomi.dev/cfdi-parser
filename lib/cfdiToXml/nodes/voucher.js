const mapAttrs = require('../../utils/mapAttrs');

const voucherAttrs = require('../constants/attributes/voucher');

function setVoucherAttrs(cfdi, voucherNode) {
  const voucherNodeAttrs = voucherAttrs.reduce(mapAttrs(cfdi), {});

  voucherNode.attr(voucherNodeAttrs);

  return voucherNode;
}

module.exports = setVoucherAttrs;
