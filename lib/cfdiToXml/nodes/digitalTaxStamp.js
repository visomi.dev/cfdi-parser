const mapAttrs = require('../../utils/mapAttrs');

const digitalTaxStampAttrs = require('../constants/attributes/digitalTaxStamp');

function setDigitalTaxStamp(digitalTaxStamp, complementNode) {
  const digitalTaxStampNode = complementNode.node('TimbreFiscalDigital');

  digitalTaxStampNode.namespace('tfd', 'http://www.sat.gob.mx/TimbreFiscalDigital');

  const digitalTaxStampNodeAttrs = digitalTaxStampAttrs.reduce(mapAttrs(digitalTaxStamp), {});

  digitalTaxStampNode.attr(digitalTaxStampNodeAttrs);

  return complementNode;
}

module.exports = setDigitalTaxStamp;
