const mapAttrs = require('../../utils/mapAttrs');

const taxesAttrs = require('../constants/attributes/taxes');
const retainedTaxAttrs = require('../constants/attributes/taxRetained');
const taxTransferAttrs = require('../constants/attributes/taxTransfer');

function setTaxes(cfdi, voucherNode) {
  if (!cfdi.taxes) return voucherNode;

  const taxesNode = voucherNode.node('Impuestos');

  const taxesNodeAttrs = taxesAttrs.reduce(mapAttrs(cfdi), {});

  taxesNode.attr(taxesNodeAttrs);
  taxesNode.namespace('cfdi');

  if (cfdi.taxes.retained && Array.isArray(cfdi.taxes.retained)) {
    const retainedNode = taxesNode.node('Retenciones');
    retainedNode.namespace('cfdi');

    cfdi.taxes.retained.forEach((retainedTax) => {
      const retainedTaxNode = retainedNode.node('Retencion');

      const retainedTaxNodeAttrs = retainedTaxAttrs.reduce(mapAttrs(retainedTax), {});

      retainedTaxNode.attr(retainedTaxNodeAttrs);
      retainedTaxNode.namespace('cfdi');
    });
  }

  if (cfdi.taxes.transfers && Array.isArray(cfdi.taxes.transfers)) {
    const transfersNode = taxesNode.node('Traslados');

    transfersNode.namespace('cfdi');

    cfdi.taxes.transfers.forEach((taxTransfer) => {
      const taxTransferNode = transfersNode.node('Traslado');

      const taxTransferNodeAttrs = taxTransferAttrs.reduce(mapAttrs(taxTransfer), {});

      taxTransferNode.attr(taxTransferNodeAttrs);
      taxTransferNode.namespace('cfdi');
    });
  }

  return voucherNode;
}

module.exports = setTaxes;
