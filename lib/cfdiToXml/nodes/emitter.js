const mapAttrs = require('../../utils/mapAttrs');

const emitterAttrs = require('../constants/attributes/emitter');

function setEmitterAttrs(cfdi, voucherNode) {
  const emitterNode = voucherNode.node('Emisor');

  const emitterNodeAttrs = emitterAttrs.reduce(mapAttrs(cfdi), {});

  emitterNode.attr(emitterNodeAttrs);
  emitterNode.namespace('cfdi');

  return voucherNode;
}

module.exports = setEmitterAttrs;
