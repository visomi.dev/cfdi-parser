const mapAttrs = require('../../../utils/mapAttrs');

const payrollDeductionAttrs = require('../../constants/attributes/payroll/deduction');

function pushPayrollDeduction(deduction, deductionsNode) {
  const deductionNode = deductionsNode.node('Deduccion');
  deductionNode.namespace('nomina12');

  const deductionNodeAttrs = payrollDeductionAttrs.reduce(mapAttrs(deduction), {});
  deductionNode.attr(deductionNodeAttrs);

  return deductionsNode;
}

module.exports = pushPayrollDeduction;
