const isEmpty = require('lodash/isEmpty');

const mapAttrs = require('../../../utils/mapAttrs');

const payrollAttrs = require('../../constants/attributes/payroll');
const payrollEmitterAttrs = require('../../constants/attributes/payroll/emitter');
const payrollReceiverAttrs = require('../../constants/attributes/payroll/receiver');
const payrollPerceptsAttrs = require('../../constants/attributes/payroll/percepts');
const payrollDeductionsAttrs = require('../../constants/attributes/payroll/deductions');

const pushPayrollPercept = require('./percept');
const pushPayrollDeduction = require('./deduction');
const pushPayrollOtherPayment = require('./otherPayment');
const pushPayrollPaidLeave = require('./paidLeave');

function setPayroll(payroll, complementNode) {
  const payrollNode = complementNode.node('Nomina');

  payrollNode.namespace('nomina12', 'http://www.sat.gob.mx/nomina12');

  const payrollNodeAttrs = payrollAttrs.reduce(mapAttrs(payroll), {});
  const payrollEmitterNodeAttrs = payrollEmitterAttrs.reduce(mapAttrs(payroll), {});
  const payrollReceiverNodeAttrs = payrollReceiverAttrs.reduce(mapAttrs(payroll), {});
  const payrollPerceptsNodeAttrs = payrollPerceptsAttrs.reduce(mapAttrs(payroll), {});
  const payrollDeductionsNodeAttrs = payrollDeductionsAttrs.reduce(mapAttrs(payroll), {});

  payrollNode.attr(payrollNodeAttrs);

  if (!isEmpty(payrollEmitterNodeAttrs)) {
    const payrollEmitterNode = payrollNode.node('Emisor');
    payrollEmitterNode.namespace('nomina12');

    payrollEmitterNode.attr(payrollEmitterNodeAttrs);
  }

  if (!isEmpty(payrollReceiverNodeAttrs)) {
    const payrollReceiverNode = payrollNode.node('Receptor');
    payrollReceiverNode.namespace('nomina12');

    payrollReceiverNode.attr(payrollReceiverNodeAttrs);
  }

  if (!isEmpty(payrollPerceptsNodeAttrs)) {
    const payrollPerceptsNode = payrollNode.node('Percepciones');
    payrollPerceptsNode.namespace('nomina12');

    payrollPerceptsNode.attr(payrollPerceptsNodeAttrs);

    if (Array.isArray(payroll.percepts)) {
      payroll.percepts.forEach((percept) => {
        pushPayrollPercept(percept, payrollPerceptsNode);
      });
    }
  }

  if (!isEmpty(payrollDeductionsNodeAttrs)) {
    const payrollDeductionsNode = payrollNode.node('Deducciones');
    payrollDeductionsNode.namespace('nomina12');

    payrollDeductionsNode.attr(payrollDeductionsNodeAttrs);

    if (Array.isArray(payroll.deductions)) {
      payroll.deductions.forEach((deduction) => {
        pushPayrollDeduction(deduction, payrollDeductionsNode);
      });
    }
  }

  if (Array.isArray(payroll.otherPayments)) {
    const payrollOtherPaymentsNode = payrollNode.node('OtrosPagos');
    payrollOtherPaymentsNode.namespace('nomina12');

    payroll.otherPayments.forEach((otherPayment) => {
      pushPayrollOtherPayment(otherPayment, payrollOtherPaymentsNode);
    });
  }

  if (Array.isArray(payroll.paidLeaves)) {
    const payrollPaidLeavesNode = payrollNode.node('Incapacidades');
    payrollPaidLeavesNode.namespace('nomina12');

    payroll.paidLeaves.forEach((paidLeave) => {
      pushPayrollPaidLeave(paidLeave, payrollPaidLeavesNode);
    });
  }

  return complementNode;
}

module.exports = setPayroll;
