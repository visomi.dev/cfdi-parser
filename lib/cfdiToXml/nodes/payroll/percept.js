const mapAttrs = require('../../../utils/mapAttrs');

const payrollPerceptAttrs = require('../../constants/attributes/payroll/percept');
const payrollOvertimeAttrs = require('../../constants/attributes/payroll/overtime');

function pushPayrollPercept(percept, perceptsNode) {
  const perceptNode = perceptsNode.node('Percepcion');
  perceptNode.namespace('nomina12');

  const perceptNodeAttrs = payrollPerceptAttrs.reduce(mapAttrs(percept), {});
  perceptNode.attr(perceptNodeAttrs);

  if (Array.isArray(percept.overtime)) {
    percept.overtime.forEach((overtime) => {
      const overtimeNode = perceptNode.node('HorasExtra');
      overtimeNode.namespace('nomina12');

      const overtimeNodeAttrs = payrollOvertimeAttrs.reduce(mapAttrs(overtime), {});

      overtimeNode.attr(overtimeNodeAttrs);
    });
  }

  return perceptsNode;
}

module.exports = pushPayrollPercept;
