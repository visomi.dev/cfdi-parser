const mapAttrs = require('../../../utils/mapAttrs');

const payrollPaidLeaveAttrs = require('../../constants/attributes/payroll/paidLeave');

function pushPayrollPaidLeave(paidLeave, paidLeavesNode) {
  const paidLeaveNode = paidLeavesNode.node('Incapacidad');
  paidLeaveNode.namespace('nomina12');

  const paidLeaveNodeAttrs = payrollPaidLeaveAttrs.reduce(mapAttrs(paidLeave), {});
  paidLeaveNode.attr(paidLeaveNodeAttrs);

  return paidLeavesNode;
}

module.exports = pushPayrollPaidLeave;
