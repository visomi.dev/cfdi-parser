const mapAttrs = require('../../../utils/mapAttrs');

const payrollOtherPaymentAttrs = require('../../constants/attributes/payroll/otherPayment');

function pushPayrollOtherPayment(otherPayment, otherPaymentsNode) {
  const otherPaymentNode = otherPaymentsNode.node('OtroPago');
  otherPaymentNode.namespace('nomina12');

  const otherPaymentNodeAttrs = payrollOtherPaymentAttrs.reduce(mapAttrs(otherPayment), {});
  otherPaymentNode.attr(otherPaymentNodeAttrs);

  if (otherPayment.otherPaymentType === '002') {
    const employmentAid = otherPaymentNode.node('SubsidioAlEmpleo');
    employmentAid.namespace('nomina12');
    employmentAid.attr({ SubsidioCausado: otherPayment.amount });
  }

  return otherPaymentsNode;
}

module.exports = pushPayrollOtherPayment;
