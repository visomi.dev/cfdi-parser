const mapAttrs = require('../../utils/mapAttrs');

const localTaxesTransfersAttrs = require('../constants/attributes/taxesLocal');
const localTaxTransferAttrs = require('../constants/attributes/taxTransferLocal');

function setLocalTaxes(localTaxes, complementNode) {
  if (!localTaxes.localTransfers) return complementNode;

  const localTaxesNode = complementNode.node('ImpuestosLocales');

  localTaxesNode.namespace('implocal', 'http://www.sat.gob.mx/implocal');

  const localTaxesNodeAttrs = localTaxesTransfersAttrs.reduce(mapAttrs(localTaxes), {});

  localTaxesNode.attr(localTaxesNodeAttrs);

  localTaxes.localTransfers.forEach((localTransfer) => {
    const localTaxNode = localTaxesNode.node('TrasladosLocales');

    localTaxNode.namespace('implocal');

    const localTaxNodeAttrs = localTaxTransferAttrs.reduce(mapAttrs(localTransfer), {});

    localTaxNode.attr(localTaxNodeAttrs);
  });

  return complementNode;
}

module.exports = setLocalTaxes;
