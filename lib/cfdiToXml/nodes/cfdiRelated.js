const RELATION_TYPE_KEY = 'TipoRelacion';

function setCfdiRelated(cfdi, voucherNode) {
  if (!cfdi.cfdisRelated && !Array.isArray(cfdi.cfdisRelated)) {
    return voucherNode;
  }

  cfdi.cfdisRelated.forEach(({ relationType, uuids }) => {
    const cfdisRelatedNode = voucherNode.node('CfdiRelacionados');

    cfdisRelatedNode.attr({ [RELATION_TYPE_KEY]: relationType });
    cfdisRelatedNode.namespace('cfdi');

    uuids.forEach((UUID) => {
      const cfdiRelatedNode = cfdisRelatedNode.node('CfdiRelacionado');
      cfdiRelatedNode.attr({ UUID });
      cfdiRelatedNode.namespace('cfdi');
    });
  });

  return voucherNode;
}

module.exports = setCfdiRelated;
