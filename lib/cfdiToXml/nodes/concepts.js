const mapAttrs = require('../../utils/mapAttrs');

const conceptAttrs = require('../constants/attributes/concept');
const taxTransferAttrs = require('../constants/attributes/taxTransfer');
const taxRetainedAttrs = require('../constants/attributes/taxRetained');

function setConcepts(cfdi, voucherNode) {
  if (!cfdi.concepts && !Array.isArray(cfdi.concepts)) return voucherNode;

  const conceptsNode = voucherNode.node('Conceptos');
  conceptsNode.namespace('cfdi');

  cfdi.concepts.forEach((concept) => {
    const conceptNode = conceptsNode.node('Concepto');
    conceptNode.namespace('cfdi');

    const conceptNodeAttrs = conceptAttrs.reduce(mapAttrs(concept), {});

    conceptNode.attr(conceptNodeAttrs);

    if (concept.retainedTaxes || concept.taxTransfers) {
      const taxesNode = conceptNode.node('Impuestos');
      taxesNode.namespace('cfdi');

      if (concept.taxTransfers && Array.isArray(concept.taxTransfers)) {
        const transfersNode = taxesNode.node('Traslados');
        transfersNode.namespace('cfdi');

        concept.taxTransfers.forEach((taxTransfer) => {
          const taxTransferNode = transfersNode.node('Traslado');

          const taxTransferNodeAttrs = taxTransferAttrs.reduce(mapAttrs(taxTransfer), {});

          taxTransferNode.attr(taxTransferNodeAttrs);
          taxTransferNode.namespace('cfdi');
        });
      }

      if (concept.retainedTaxes && Array.isArray(concept.retainedTaxes)) {
        const retainedNode = taxesNode.node('Retenciones');
        retainedNode.namespace('cfdi');

        concept.retainedTaxes.forEach((retainedTax) => {
          const retainedTaxNode = retainedNode.node('Retencion');

          const retainedTaxNodeAttrs = taxRetainedAttrs.reduce(mapAttrs(retainedTax), {});

          retainedTaxNode.attr(retainedTaxNodeAttrs);
          retainedTaxNode.namespace('cfdi');
        });
      }
    }
  });

  return voucherNode;
}

module.exports = setConcepts;
