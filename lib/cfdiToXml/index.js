const libxmljs = require('libxmljs');

const cfdiSkeleton = require('./constants/skeleton');

const setVoucherAttrs = require('./nodes/voucher');
const setCfdiRelated = require('./nodes/cfdiRelated');
const setEmitterAttrs = require('./nodes/emitter');
const setReceiverAttrs = require('./nodes/receiver');
const setConcepts = require('./nodes/concepts');
const setTaxes = require('./nodes/taxes');
const setComplement = require('./nodes/complement');

async function cfdiToXml(cfdi, xmlDocObj = false) {
  const xmlDoc = libxmljs.parseXml(cfdiSkeleton, {
    noblanks: true,
    recover: true,
    noerror: true,
    nonet: true,
    nsclean: false,
    dtdload: false,
    dtdvalid: false,
    old: true,
  });

  const voucherNode = xmlDoc.get('//*[local-name()="Comprobante"]');

  setVoucherAttrs(cfdi, voucherNode);
  setCfdiRelated(cfdi, voucherNode);
  setEmitterAttrs(cfdi, voucherNode);
  setReceiverAttrs(cfdi, voucherNode);
  setConcepts(cfdi, voucherNode);
  setTaxes(cfdi, voucherNode);
  setComplement(cfdi, voucherNode);

  if (xmlDocObj) return xmlDoc;
  return xmlDoc.toString({ format: true, xml: true });
}

module.exports = cfdiToXml;
