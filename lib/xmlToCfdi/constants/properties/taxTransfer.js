const TAX_TRANSFER_PROPS = [
  {
    name: 'tipoFactor',
    key: 'factorType',
  },
  {
    name: 'tasaOCuota',
    key: 'fee',
  },
  {
    name: 'impuesto',
    key: 'tax',
  },
  {
    name: 'base',
    key: 'base',
  },
  {
    name: 'importe',
    key: 'amount',
    cast: (value) => parseFloat(value),
  },
];

module.exports = TAX_TRANSFER_PROPS;
