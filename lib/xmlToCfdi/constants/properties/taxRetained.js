const RETAINED_TAX_PROPS = [
  {
    name: 'tipoFactor',
    key: 'factorType',
  },
  {
    name: 'tasaOCuota',
    key: 'fee',
  },
  {
    name: 'impuesto',
    key: 'tax',
  },
  {
    name: 'base',
    key: 'base',
  },
  {
    name: 'importe',
    key: 'amount',
    cast: (value) => parseFloat(value),
  },
];

module.exports = RETAINED_TAX_PROPS;
