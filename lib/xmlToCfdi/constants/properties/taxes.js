const TAXES_PROPS = ['Impuestos', [
  {
    name: 'totalImpuestosRetenidos',
    key: 'totalTaxesWithheld',
    cast: (value) => parseFloat(value),
  },
  {
    name: 'totalImpuestosTrasladados',
    key: 'totalTaxesTransferred',
    cast: (value) => parseFloat(value),
  },
]];

module.exports = TAXES_PROPS;
