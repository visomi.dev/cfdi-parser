const EMITTER_PROPS = ['Emisor', [
  { name: 'regimenFiscal', key: 'emitterRegime' },
  { name: 'nombre', key: 'emitterName' },
  { name: 'rfc', key: 'emitterRfc' },
]];

module.exports = EMITTER_PROPS;
