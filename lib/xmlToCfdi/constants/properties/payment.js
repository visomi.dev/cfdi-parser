const PAYMENT_PROPS = [
  {
    name: 'ctaBeneficiario',
    key: 'receiverAccount',
  },
  {
    name: 'tipoCadPago',
    key: 'stringPaymentType',
  },
  {
    name: 'numOperacion',
    key: 'operationNumber',
  },
  {
    name: 'monto',
    key: 'amount',
    type: 'float',
  },
  {
    name: 'rfcEmisorCtaBen',
    key: 'receiverRfc',
  },
  {
    name: 'ctaOrdenante',
    key: 'issuingAccount',
  },
  {
    name: 'rfcEmisorCtaOrd',
    key: 'issuingRfc',
  },
  {
    name: 'tipoCambioP',
    key: 'exchangeRate',
  },
  {
    name: 'nomBancoOrdExt',
    key: 'bankName',
  },
  {
    name: 'certPago',
    key: 'certificate',
  },
  {
    name: 'formaDePagoP',
    key: 'wayPay',
  },
  {
    name: 'monedaP',
    key: 'currency',
  },
  {
    name: 'cadPago',
    key: 'string',
  },
  {
    name: 'selloPago',
    key: 'seal',
  },
];

module.exports = PAYMENT_PROPS;
