const VOUCHER_PROPS = require('./voucher');
const EMITTER_PROPS = require('./emitter');
const RECEIVER_PROPS = require('./receiver');
const TAXES_PROPS = require('./taxes');

const PLAIN_PROPS = [
  VOUCHER_PROPS,
  EMITTER_PROPS,
  RECEIVER_PROPS,
  TAXES_PROPS,
];

module.exports = PLAIN_PROPS;
