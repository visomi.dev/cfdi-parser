const moment = require('moment');

const VOUCHER_PROPS = ['Comprobante', [
  { name: 'noCertificado', key: 'voucherCertificateNumber' },
  { name: 'lugarExpedicion', key: 'voucherExpeditionPlace' },
  { name: 'tipoCambio', key: 'voucherExchangeRate' },
  { name: 'tipoDeComprobante', key: 'voucherType' },
  { name: 'descuento', key: 'voucherDiscount' },
  { name: 'folio', key: 'voucherConsecutive' },
  { name: 'moneda', key: 'voucherCurrency' },
  { name: 'serie', key: 'voucherSeries' },

  { name: 'formaDePago', key: 'voucherPaymentType' },
  { name: 'formaPago', key: 'voucherPaymentType' },

  { name: 'metodoDePago', key: 'voucherPaymentMethod' },
  { name: 'metodoPago', key: 'voucherPaymentMethod' },

  { name: 'certificado', key: 'voucherCertificate' },
  { name: 'confirmacion', key: 'voucherConfirmation' },
  { name: 'version', key: 'voucherVersion' },
  { name: 'sello', key: 'voucherSeal' },
  { name: 'UUID', key: 'voucherUuid' },
  {
    name: 'total',
    key: 'voucherTotal',
    cast: (value) => parseFloat(value),
  },
  {
    name: 'subTotal',
    key: 'voucherSubTotal',
    cast: (value) => parseFloat(value),
  },
  {
    name: 'fecha',
    key: 'voucherDate',
    cast: (value) => moment(value).unix(),
  },
]];

module.exports = VOUCHER_PROPS;
