const moment = require('moment');

const DIGITAL_TAX_STAMP_PROPS = [
  { name: 'noCertificadoSAT', key: 'satCertificateNumber' },
  { name: 'rfcProvCertif', key: 'rfcProvCertif' },
  { name: 'selloSAT', key: 'satSeal' },
  { name: 'leyenda', key: 'legend' },
  { name: 'selloCFD', key: 'cfdSeal' },
  { name: 'UUID', key: 'uuid' },
  { name: 'version', key: 'version' },
  {
    name: 'fechaTimbrado',
    key: 'datetimeStamp',
    cast: (value) => moment(value).unix(),
  },
];

module.exports = DIGITAL_TAX_STAMP_PROPS;
