const CONCEPT_PROPS = [
  { name: 'claveProdServ', key: 'productServiceKey' },
  { name: 'noIdentificacion', key: 'identificationNumber' },
  { name: 'descripcion', key: 'description' },
  { name: 'claveUnidad', key: 'unitKey' },
  { name: 'valorUnitario', key: 'unit' },
  { name: 'descuento', key: 'discount' },
  { name: 'cantidad', key: 'quantity' },
  {
    name: 'importe',
    key: 'amount',
    cast: (value) => parseFloat(value),
  },
];

module.exports = CONCEPT_PROPS;
