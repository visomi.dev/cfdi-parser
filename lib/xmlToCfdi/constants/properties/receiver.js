const RECEIVER_PROPS = ['Receptor', [
  { name: 'residenciaFiscal', key: 'receiverFiscalResidence' },
  { name: 'numRegIdTrib', key: 'receiverNumRegIdTrib' },
  { name: 'usoCFDI', key: 'receiverCfdiUsage' },
  { name: 'nombre', key: 'receiverName' },
  { name: 'rfc', key: 'receiverRfc' },
]];

module.exports = RECEIVER_PROPS;
