const merge = require('lodash/merge');

const setDigitalTaxStamp = require('./digitalTaxStamp');
const setPayments = require('./payments');

function setComplement(xmlDoc, model = {}) {
  const complementNode = xmlDoc.get('//*[local-name()="Complemento"]');
  if (!complementNode) return model;

  const complement = {};

  setDigitalTaxStamp(xmlDoc, complement);
  setPayments(xmlDoc, complement);

  return merge(model, { complement });
}

module.exports = setComplement;
