const merge = require('lodash/merge');
const set = require('lodash/set');

const getAttr = require('../../utils/getAttr');

const plainProps = require('../constants/properties/plain');

function setPlainProps(xmlDoc, model = {}) {
  const baseProps = plainProps.reduce((accum, [node, props]) => {
    const xmlNode = node === 'Comprobante'
      ? xmlDoc.get('/*[local-name()="Comprobante"]')
      : xmlDoc.get(`/*[local-name()="Comprobante"]/*[local-name()="${node}"]`);

    if (xmlNode) {
      props.forEach((prop) => {
        const attr = getAttr(xmlNode, prop.name);

        if (attr) {
          if (attr.value()) {
            const value = prop.cast ? prop.cast(attr.value()) : attr.value();

            set(accum, prop.key, value);
          }
        }
      });
    }

    return accum;
  }, {});

  return merge(model, baseProps);
}

module.exports = setPlainProps;
