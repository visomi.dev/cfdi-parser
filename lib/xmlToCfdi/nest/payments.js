const isEmpty = require('lodash/isEmpty');
const set = require('lodash/set');

const xmlNodeToJs = require('../../utils/xmlNodeToJs');

const paymentProps = require('../constants/properties/payment');

function setPayments(xmlDoc, complement = {}) {
  const paymentsNodes = xmlDoc.find('//*[local-name()="Pago"]');

  if (isEmpty(paymentsNodes)) return complement;

  const payments = paymentsNodes.map((paymentNode) => {
    const payment = paymentProps.reduce(xmlNodeToJs(paymentNode), {});

    return payment;
  });

  set(complement, 'payments', payments);

  return complement;
}

module.exports = setPayments;
