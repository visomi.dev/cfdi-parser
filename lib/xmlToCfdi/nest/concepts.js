const isEmpty = require('lodash/isEmpty');
const set = require('lodash/set');

const xmlNodeToJs = require('../../utils/xmlNodeToJs');

const conceptProps = require('../constants/properties/concept');
const taxTransferProps = require('../constants/properties/taxTransfer');
const retainedTaxProps = require('../constants/properties/taxRetained');

function setConcepts(xmlDoc, model = {}) {
  const conceptsContainer = xmlDoc.find('//*[local-name()="Concepto"]');

  if (isEmpty(conceptsContainer)) return model;

  const concepts = conceptsContainer.map((conceptNode) => {
    const concept = conceptProps.reduce(xmlNodeToJs(conceptNode), {});

    const taxTransfersContainers = conceptNode.find('.//*[local-name()="Traslado"]');

    if (!isEmpty(taxTransfersContainers)) {
      const taxTransfers = taxTransfersContainers.map((taxTransferNode) => (
        taxTransferProps.reduce(xmlNodeToJs(taxTransferNode), {})
      ));

      set(concept, 'taxTransfers', taxTransfers);
    }

    const retainedTaxesContainers = conceptNode.find('.//*[local-name()="Retencion"]');

    if (!isEmpty(retainedTaxesContainers)) {
      const retainedTaxes = retainedTaxesContainers.map((retainedTaxNode) => (
        retainedTaxProps.reduce(xmlNodeToJs(retainedTaxNode), {})
      ));

      set(concept, 'retainedTaxes', retainedTaxes);
    }

    return concept;
  });

  if (!isEmpty(concepts)) set(model, 'concepts', concepts);

  return model;
}

module.exports = setConcepts;
