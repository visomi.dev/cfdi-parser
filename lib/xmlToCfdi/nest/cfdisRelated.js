const isEmpty = require('lodash/isEmpty');
const set = require('lodash/set');

const getAttr = require('../../utils/getAttr');

function setCfdisRelated(xmlDoc, model = {}) {
  const cfdisRelatedContainer = xmlDoc.find('//*[local-name()="CfdiRelacionados"]');

  if (isEmpty(cfdisRelatedContainer)) return model;

  const related = cfdisRelatedContainer.map((cfdisRelated) => {
    const attr = getAttr(cfdisRelated, 'tipoRelacion');

    const uuids = cfdisRelated.childNodes().map((node) => {
      const uuid = node.attr('UUID') || node.attr('uuid');

      return uuid.value();
    });

    return {
      relationType: attr.value(),
      uuids,
    };
  });

  set(model, 'cfdisRelated', related);

  return model;
}

module.exports = setCfdisRelated;
