const set = require('lodash/set');

const xmlNodeToJs = require('../../utils/xmlNodeToJs');

const digitalTaxStampProps = require('../constants/properties/digitalTaxStamp');

function setDigitalTaxStamp(xmlDoc, complement = {}) {
  const digitalTaxStampNode = xmlDoc.get('//*[local-name()="TimbreFiscalDigital"]');

  if (digitalTaxStampNode) {
    const digitalTaxStamp = digitalTaxStampProps.reduce(
      xmlNodeToJs(digitalTaxStampNode),
      {},
    );

    set(complement, 'digitalTaxStamp', digitalTaxStamp);
  }

  return complement;
}

module.exports = setDigitalTaxStamp;
