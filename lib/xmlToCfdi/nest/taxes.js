const isEmpty = require('lodash/isEmpty');
const set = require('lodash/set');

const xmlNodeToJs = require('../../utils/xmlNodeToJs');

const taxTransferProps = require('../constants/properties/taxTransfer');
const retainedTaxProps = require('../constants/properties/taxRetained');

function setTaxes(xmlDoc, model = {}) {
  const taxesNode = xmlDoc.get('/*[local-name()="Comprobante"]/*[local-name()="Impuestos"]');

  if (!taxesNode) return model;

  const taxTransfersContainers = taxesNode.find('.//*[local-name()="Traslado"]');

  if (!isEmpty(taxTransfersContainers)) {
    const taxTransfers = taxTransfersContainers.map((taxTransferNode) => (
      taxTransferProps.reduce(xmlNodeToJs(taxTransferNode), {})
    ));

    set(model, 'taxes.transfers', taxTransfers);
  }

  const retainedTaxesContainers = taxesNode.find('.//*[local-name()="Retencion"]');

  if (!isEmpty(retainedTaxesContainers)) {
    const retainedTaxes = retainedTaxesContainers.map((retainedTaxNode) => (
      retainedTaxProps.reduce(xmlNodeToJs(retainedTaxNode), {})
    ));

    set(model, 'taxes.retained', retainedTaxes);
  }

  return model;
}

module.exports = setTaxes;
