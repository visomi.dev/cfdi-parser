const libxmljs = require('libxmljs');

const setPlainProps = require('./nest/plain');
const setCfdisRelated = require('./nest/cfdisRelated');
const setConcepts = require('./nest/concepts');
const setComplement = require('./nest/complement');
const setTaxes = require('./nest/taxes');

function xmlToCfdi(xmlString) {
  const cfdi = {};

  const xmlDoc = libxmljs.parseXmlString(xmlString, {
    noblanks: true,
    recover: true,
    noerror: true,
    nonet: true,
    nsclean: false,
    dtdload: false,
    dtdvalid: false,
    old: true,
  });

  setPlainProps(xmlDoc, cfdi);
  setCfdisRelated(xmlDoc, cfdi);
  setConcepts(xmlDoc, cfdi);
  setComplement(xmlDoc, cfdi);
  setTaxes(xmlDoc, cfdi);

  return cfdi;
}

module.exports = xmlToCfdi;
