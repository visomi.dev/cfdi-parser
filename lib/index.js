const xmlToCfdi = require('./xmlToCfdi');
const cfdiToXml = require('./cfdiToXml');

module.exports.xmlToCfdi = xmlToCfdi;
module.exports.cfdiToXml = cfdiToXml;

module.exports = {
  xmlToCfdi,
  cfdiToXml,
};
