const upperFirst = require('lodash/upperFirst');

const getAttr = (node, propName) => node.attr(upperFirst(propName)) || node.attr(propName);

module.exports = getAttr;
