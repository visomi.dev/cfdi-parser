const set = require('lodash/set');

const getAttr = require('./getAttr');

const xmlNodeToJs = (node) => (accum, prop) => {
  const attr = getAttr(node, prop.name);

  if (attr) {
    if (attr.value()) {
      const value = prop.cast ? prop.cast(attr.value()) : attr.value();

      set(accum, prop.key, value);
    }
  }
  return accum;
};

module.exports = xmlNodeToJs;
