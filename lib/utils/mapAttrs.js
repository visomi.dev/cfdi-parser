const set = require('lodash/set');

function mapAttrs(values) {
  return (acumm, options) => {
    const {
      attr,
      key,
      cast,
      default: defaultValue,
    } = options;

    if (values[key]) {
      const value = cast ? cast(values[key]) : values[key];
      set(acumm, attr, value);
    } else if (defaultValue) {
      set(acumm, attr, defaultValue);
    }

    return acumm;
  };
}

module.exports = mapAttrs;
