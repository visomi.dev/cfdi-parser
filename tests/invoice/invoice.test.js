/* eslint-env node, mocha */

const fs = require('fs');
const path = require('path');
const assert = require('assert');

const cfdiParser = require('../../lib');

const json = require('./invoice.json');

const xml = fs.readFileSync(path.join(__dirname, 'invoice.xml')).toString();

describe('CFDI TYPE=I (invoice)', () => {
  describe('XML to JSON', () => {
    it('Parse XML CFDI 3.3 without errors', async () => {
      await cfdiParser.xmlToCfdi(xml);
    });

    it('Parsed must be equal to result', async () => {
      const jsonParsed = await cfdiParser.xmlToCfdi(xml);
      const xmlParsed = await cfdiParser.cfdiToXml(jsonParsed);

      assert.equal(xmlParsed, xml.toString());
    });
  });

  describe('JSON to XML', () => {
    it('Parse JSON CFDI 3.3 to XML without errors', async () => {
      await cfdiParser.cfdiToXml(json);
    });

    it('Parsed must be equal to result', async () => {
      const xmlParsed = await cfdiParser.cfdiToXml(json);
      const jsonParsed = await cfdiParser.xmlToCfdi(xmlParsed);

      assert.equal(jsonParsed.toString(), json);
    });
  });
});
